/************************************************************************************************
 * Class Name  : FeedbackController_Test.cls
 * Description : The below test class is written for FeedbackController.cls
 *				
 * Created Date: 03/2/2017
 * Author      : Ravi Katragunta
 * Version     : 1.0
 ************************************************************************************************/
@isTest
private class FeedbackController_Test {
@isTest
	private static void PostiveTestCase() {
	//Create test system admin user.
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u2 = new User(Alias = 'newUser', Email='newuser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='newuserxxyz@testorgvisa.com');
        // Positive Scenario
        System.runAs(u2){
			

				Feedback__c fb = new Feedback__c(Name='Test Feedback',Application_Name__c='VCSA',Rating__c=2.0,Comments__c='Testing');
				String Status=FeedbackController.updateRating(fb);	
                System.assert(Status=='SUCCESS');

		}
	}
		@isTest
		private static void Negative() {
		//Create test system admin user.
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u2 = new User(Alias = 'newUser', Email='newuser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='newuserxxyz@testorgvisa.com');
        // Negative Scenario
        System.runAs(u2){
				Feedback__c fb = new Feedback__c(Name=null,Application_Name__c='VCSA',Rating__c=2.0,Comments__c='Testing');
				String Status=FeedbackController.updateRating(fb);
            	System.assert(Status=='FALSE');
		
		}
		
		


	}
}