public class FeedbackController { 
 // Used to update Feedback record with new rating.
    // Params: { Feedback }
    @AuraEnabled
    public static String updateRating(Feedback__c fb ){
    	if(fb.Name!=null)
        {	
        	insert fb;
        	return 'SUCCESS';
        }
        else
        {
        	return 'FALSE';
        }
    }
}