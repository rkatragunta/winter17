public class AccountsController {
  @AuraEnabled
  public static List<Account> getAccounts() {
  //testing AddComment Branch
    return [SELECT Id, name, industry, Type, NumberOfEmployees, TickerSymbol, Phone
    FROM Account ORDER BY createdDate ASC];
  }
}