({
	doInit : function(component, event, helper) {

		// load raty rating plugin.
        var ratingElement = component.find("starRating").getElement();
        helper.loadRatingElement( component, helper, ratingElement );
        

            // update current rating attribute and set raty with current rating.
			//component.set("v.fb.Rating__c", 0);

            $(ratingElement).raty('set', { score: 0 });
            $(".star-rating, .loading-div, .footer-contents").toggle();

		
	},
	submit: function(cmp, evt, helper) {
	helper.updateRating(cmp);
	}
})