({
    loadRatingElement: function(component, helper, ratingElement){
        $( ratingElement ).raty({
            starOff  : '/resource/RatingPlugin/images/star_off_darkgray.png',
            starOn   : '/resource/RatingPlugin/images/star_on.png',
            click: function(score, evt) {

                        component.set("v.fb.Rating__c", score);
                       
                
            }
           
        });
    },
	updateRating : function( component ){
    	
        var action = component.get("c.updateRating");
        
        var iserror=false;
        var functionalityname = component.find("functionalityname");
        if($A.util.isEmpty(functionalityname.get("v.value"))) {
        iserror=true;
        functionalityname.set("v.errors", [{message:"Functionality name can't be blank"}]);
        }
        else {
        functionalityname.set("v.errors", null);
        }
		
        if(iserror==false)
        {
            var appname = component.get("v.AppName");
        	var feedback = component.get("v.fb");
        	feedback.Application_Name__c=component.get("v.AppName");
        	component.set("v.fb",feedback);
            
            action.setParams({
            fb : component.get("v.fb")
        });
        action.setCallback(this, function( response ){
            component.set("v.isVisible", true);

        });
        }
        
        $A.enqueueAction(action);
    }}
})