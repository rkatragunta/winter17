({
	handleSaveContact : function(component, event, helper) {
        
        // Prepare the action to create the new contact
            var saveContactAction = component.get("c.CreateContact");
            saveContactAction.setParams({
                c : component.get("v.newContact"),
                AccountId :	component.get("v.actid")
                
            });

            // Configure the response handler for the action
            saveContactAction.setCallback(this, function(response) {
                alert('Success');
                   
            });

            // Send the request to create the new contact
            $A.enqueueAction(saveContactAction);
            
	}
})